const app = require('../app');
const request = require('supertest');


//test to create single employee 

describe('POST /employee', function () {
    let data = {
        "name": "Sebastian",
        "email": "sebastian@gmail.com",
        "number": "9988776655",
        "homeaddress": "patrastreet,berhampur,ganjam,odisha,760009",
        "officeaddress": "newtown ,usa"

    }
    it('respond with 200 created', function (done) {
        request(app)
            .post('/employee')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});


//test to fetch all employeee

describe('GET /employee', function () {
    it('respond with 200 to fetch all employee', function (done) {
        request(app)
            .get('/employee')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

//to get single employee details
describe('GET /employee/:id', function () {
    it('respond with 200 created single employeee', function (done) {
        request(app)
            .get('/employee/5e9fd1d5f69e93288ac07820')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});


//to update single employee details
describe('PUT /employee/:id', function () {
    let data = {
        "name": "Asish kumar ratha",
        "email": "asish@gmail.com",
        "number": "3344551122",
        "homeaddress": "jenastreet,berhampur,ganjam,odisha,760003",
        "officeaddress": "huston ,usa"
        
    }
    it('respond with 200 if updated', function (done) {
        request(app)
            .put('/employee/5e9fd1d5f69e93288ac07820')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});


//delete single employee
describe('DELETE /employee/:id', function () {
 
    it('respond with 200 delete single user', function (done) {
        request(app)
            .delete('/employee/5e9fd2ac08a1ab298fbbe815')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});

