'use strict';
require('dotenv').config()
var express = require('express');
var logger = require('morgan');
var path=require("path")
var cors=require("cors");
var bodyParser = require('body-parser');
require("./db/db")
//var book = require('./routes/book');
var app = express();
app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static((__dirname,'public')));
//app.get("/",function(req,res){
//  res.send("welcome s rama laxmi narayan patro")});
app.use("/employee",require("./routes/employeeroute"))
//app.listen("2222",function(){
//    console.log("server listning port no 2222");
//})
module.exports=app;