var mongoose = require('mongoose');

var EmployeeSchema = new mongoose.Schema({
  name: String,
  email: String,
  number: String,
  homeaddress: String,
  officeaddress:String,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('EmployeeSchema', EmployeeSchema);
