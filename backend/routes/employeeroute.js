'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Employee = require('../models/employeemodel');
require("../db/db")

//get all employee
router.get('/', function(req, res) {
  Employee.find().then(function(resolved){
      res.status(200).json(resolved);
  }).catch(function(rejected){
      res.send(rejected);
  })
});

//find single employee
router.get('/:id', function(req, res) {
    console.log(req.params.id)
  Employee.findById(req.params.id).then(function(resolved){
      res.status(200).send(resolved);
  }).catch(function(rejected){
      res.send(rejected);
  })
});

//post employee details
router.post('/', function(req, res, next) {
    console.log(req.body)
    Employee.create(req.body).then(function(resolved){
    if(resolved)  
    res.status(200).send(resolved)
     }).catch(function(rejected){
          res.send(rejected)}) });


//update one  employee
router.put('/:id', function(req, res, next) {
  console.log(req.params.id);
  console.log(req.body.obj);
  Employee.findByIdAndUpdate(req.params.id, req.body).then(function(resolved){
      res.staus(200).send(resolved)
  }).catch(function(rejected){
      res.send(rejected)
  })
});

//delete one employee
router.delete('/:id', function(req, res, next) {
  Employee.findByIdAndRemove(req.params.id).then(function(resolved){
      res.status(200).send(resolved);
  }).catch(function(rejected){
      res.send(rejected)
  })
});

module.exports = router;
