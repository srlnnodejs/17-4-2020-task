import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateemployeeComponent} from "./components/createemployee/createemployee.component"
import {DeleteemployeeComponent} from "./components/deleteemployee/deleteemployee.component";
import {DetailsemployeeComponent} from "./components/detailsemployee/detailsemployee.component"
import {EmployeelistComponent} from "./components/employeelist/employeelist.component";
import {UpdateemployeeComponent} from "./components/updateemployee/updateemployee.component"
const routes: Routes = [
{path:'',redirectTo:"/list",pathMatch:'full'},
{path:"create",component:CreateemployeeComponent},
{path:"delete",component:DeleteemployeeComponent},
{path:"details/:_id",component:DetailsemployeeComponent},
{path:"list",component:EmployeelistComponent},
{path:"update/:_id",component:UpdateemployeeComponent}];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingdata=[CreateemployeeComponent
,DeleteemployeeComponent,DetailsemployeeComponent,
EmployeelistComponent,UpdateemployeeComponent]