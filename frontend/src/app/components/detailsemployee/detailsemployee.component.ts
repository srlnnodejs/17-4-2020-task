import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from "@angular/router"
import {CrudService} from "../../services/crud.service"
@Component({
  selector: 'app-detailsemployee',
  templateUrl: './detailsemployee.component.html',
  styleUrls: ['./detailsemployee.component.css']
})
export class DetailsemployeeComponent implements OnInit {
public employeeid;
public employeedetails:any;  
constructor(private _activatedroute:ActivatedRoute,
  private _employeedata:CrudService,
  private _router:Router) { }


//update single employee 
updateuser(){
this._router.navigate(['/update',this._activatedroute.snapshot.paramMap.get("_id")]);
}

//delte single user
deleteuser(){
this._employeedata.deleteEmployees(this._activatedroute.snapshot.paramMap.get("_id")).
subscribe(res=>
  {
    if(res){
      this._router.navigate(["/list"])
    }
    console.log(this.employeedetails)
  },err=>console.log(err));}


  //get employee details from server
  ngOnInit(): void {
  //this.employeeid=parseInt(this._activatedroute.snapshot.paramMap.get("_id"));
  //to get employee details
  this.employeedetails=
  this._employeedata.
  aboutEmployees(this._activatedroute.snapshot.paramMap.get("_id")).
  subscribe(res=>
    {
      this.employeedetails=res;
      console.log(this.employeedetails)
    }
    ,
    err=>console.log(err));
  }

}

