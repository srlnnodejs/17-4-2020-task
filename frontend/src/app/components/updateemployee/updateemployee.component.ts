import { Component, OnInit } from '@angular/core';
import {CrudService} from "../../services/crud.service"
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';
import {ActivatedRoute,Router} from "@angular/router"
@Component({
  selector: 'app-updateemployee',
  templateUrl: './updateemployee.component.html',
  styleUrls: ['./updateemployee.component.css']
})
export class UpdateemployeeComponent implements OnInit {
  updatedata:any;
  updateForm:FormGroup;
  updateid:any;
  constructor(private _updateEmployee:CrudService,
    private _router:Router,
    private _activatedroute:ActivatedRoute) { }
    ngOnInit(): void {
    this.updateForm=new FormGroup({
      name:new FormControl(),
      email:new FormControl(),
      number:new FormControl(),
      homeaddress:new FormControl(),
      officeaddress:new FormControl()})}
      
      updateuser(){
        console.log(this.updateForm.getRawValue());
        console.log(this._activatedroute.snapshot.paramMap.get("_id"))
        this._updateEmployee.updateEmployee(
      {"id":this._activatedroute.snapshot.paramMap.get("_id"),
      "data":this.updateForm.getRawValue()}).
        subscribe(res=>
          {if(res){
              this._router.navigate(["/list"])
            }
          }
          ,
          err=>console.log(err));
      }

}
