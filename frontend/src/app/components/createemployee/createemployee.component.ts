
import { Component, OnInit } from '@angular/core';
import {CrudService} from "../../services/crud.service"
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';
import {Router} from "@angular/router"
@Component({
  selector: 'app-createemployee',
  templateUrl: './createemployee.component.html',
  styleUrls: ['./createemployee.component.css']
})
export class CreateemployeeComponent implements OnInit {
  employeeForm:FormGroup;
  constructor(private _newEmployee:CrudService,private _router:Router) { }
  employeeStatus:any;
  ngOnInit(): void {
  this.employeeForm=new FormGroup({
    name:new FormControl(),
    email:new FormControl(),
    number:new FormControl(),
    homeaddress:new FormControl(),
    officeaddress:new FormControl()})}

    saveuser(){
      console.log(this.employeeForm.getRawValue());
      this._newEmployee.createEmployee(this.employeeForm.getRawValue()).
      subscribe(res=>
      
        {
          console.log(res)
          if(res){
            this._router.navigate(["/list"])
          }
        }
        ,
        err=>console.log(err));
      

    }
}
