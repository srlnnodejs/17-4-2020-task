import { Component, OnInit } from '@angular/core';
import {CrudService} from "../../services/crud.service"
import {Router} from "@angular/router"

@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css']
})
export class EmployeelistComponent implements OnInit {
Employeeslist:any;
  
constructor(private _listofemployees:CrudService,private _router:Router) { }
  
onSelect(employee){
      console.log(employee._id)
      this._router.navigate(['/details',employee._id])
  }
onCreate(){
    console.log()
    this._router.navigate(['/create'])
}

ngOnInit(): void {
    this._listofemployees.listofEmployees().
    subscribe(res=>
      {
        this.Employeeslist=res
        console.log(this.Employeeslist)
      }
      ,
      err=>console.log(err));
  }

}
