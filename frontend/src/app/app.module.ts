import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http"
import { AppRoutingModule,routingdata } from './app-routing.module';
import { AppComponent } from './app.component';
import {CrudService} from "./services/crud.service";
import {ReactiveFormsModule} from "@angular/forms"

@NgModule({
  declarations: [
    AppComponent,
    routingdata
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,ReactiveFormsModule
  ],
  providers: [CrudService],
  bootstrap: [AppComponent]
})
export class AppModule { }
