import {Injectable} from "@angular/core";
import {Http,Response} from "@angular/http";
import "rxjs/Rx";
import { HttpClient } from '@angular/common/http'
import {Observable} from "rxjs/Observable";
@Injectable({
  providedIn: 'root'
})
export class CrudService {
  constructor(private _http: HttpClient) { }
  
  //all employees details
  public listofEmployees(){
    return this._http.get("http://localhost:2222/Employee")
        .map((res:Response)=>{
            return res;
    }).catch(this._handleError);
}

//create single employee
public createEmployee(obj:any){
  return this._http.post("http://localhost:2222/Employee",obj)
      .map((res:Response)=>{
          return res;
  }).catch(this._handleError);
}

//update employeee
public updateEmployee(obj:any){
  console.log(obj)
  return this._http.put("http://localhost:2222/Employee/"+obj.id,obj.data)
      .map((res:Response)=>{
          return res;
  }).catch(this._handleError);
}
//
public aboutEmployees(id){
  return this._http.get("http://localhost:2222/Employee/"+id)
      .map((res:Response)=>{
          return res;
  }).catch(this._handleError);
}
public deleteEmployees(id){
  console.log(id)
  return this._http.delete("http://localhost:2222/Employee/"+id)
      .map((res:Response)=>{
          return res;
  }).catch(this._handleError);
}


public _handleError(error){
  console.error("Error...."+error);
  return Observable.throw(error || "Internal Server Error...!");
}}
